$(document).ready(function() {

    // Listen for ENTER key press in input box
    $("#display").keyup(function(e) {
        if (e.keyCode == 13) {
            processGuess();
        }
    });

    // Capture all key inputs and send to search box
    $(document).keydown(function() {
        clearIfAlreadyGuessed();

        var focused = $('input:focus');
        if (!focused.length) {
            $('#display').focus();
        }
    });

    // Listen for button presses
    $('.numpad button').click(function() {
        switch ($(this).attr('id')) {
            case 'go':
                processGuess();
                break;
            case 'clear':
                clearDisplay();
                break;
            default:
                clearIfAlreadyGuessed();
                var num = $(this).text();
                addToDisplay(num);
        }
    });

    // Trigger game
    runGame();

});

// Setup vars
var initial, min, max, secret;
var tries = 0,
    score = 0,
    played = 0;

var alreadyGuessed = false;
var guessHistory = [];

var higherMsg = '<i class="fa fa-arrow-up mr10 red"></i> Guess higher';
var lowerMsg = '<i class="fa fa-arrow-down mr10 red"></i> Guess lower';
var successMsg = '<i class="fa fa-check mr10 green"></i> Success!';

var invalidGuessMsg = '<span class="red"><i class="fa fa-exclamation-circle mr10"></i> Invalid guess</span>';
var outOfRangeMsg = '<span class="red"><i class="fa fa-exclamation-circle mr10"></i> Out of range</span>';

var errorMsg = '<span class="red"><i class="fa fa-exclamation-circle mr10"></i> Error evaluating your guess</span>';

/**
 * Main game process
 */
function runGame() {
    clearDisplay();
    updateTries({
        reset: true
    });

    initial = getRandomNum(1, 999);

    if (initial >= 101) {
        min = initial - 100;
        max = initial;
    } else {
        min = initial;
        max = initial + 100;
    }

    secret = getRandomNum(min, max);
    // console.log(secret);

    showRanges(min, max);
}

/**
 * Add pressed key to display
 */
function addToDisplay(num) {
    var content = $('#display').val();
    if (content.length == 3) {
        return;
    }

    content = content + ('' + num);
    $('#display').val(content);
}

/**
 * Clear the display
 */
function clearDisplay() {
    $('#display').val('');
}

/**
 * Process user's guess
 */
function processGuess() {
    alreadyGuessed = true;
    var guess = $('#display').val();

    if (isNaN(guess) || guess === '' || guess == ' ') {
        giveFeedback('invalid-guess');
        return;
    }
    if (! isInRange(guess, min, max)) {
        giveFeedback('out-of-range');
        return;
    }

    guessHistory.push(guess);

    updateTries({
        add: true
    });

    if (guess == secret) {
        giveFeedback('success');
    } else if (guess > secret) {
        giveFeedback('lower');
    } else {
        giveFeedback('higher');
    }    
}

/**
 * Clear the input display
 * if user has guessed before
 */
function clearIfAlreadyGuessed() {
    if (alreadyGuessed) {
        clearDisplay();
        alreadyGuessed = false;
    }
}

/**
 * Give a response after a guess is made
 * based on whether guess is correct,
 * incorrect (higher or lower)
 */
function giveFeedback(type) {
    switch (type) {
        case 'success':
            $('#feedback').html(successMsg);
            break;
        case 'higher':
            $('#feedback').html(higherMsg);
            break;
        case 'lower':
            $('#feedback').html(lowerMsg);
            break;
        case 'out-of-range':
            $('#feedback').html(outOfRangeMsg);
            break;
        case 'invalid-guess':
            $('#feedback').html(invalidGuessMsg);
            break;
        default:
            clearDisplay();
            $('#feedback').html(errorMsg);
    }
    if (type == 'success') {
        $('#feedback').slideDown();
        window.setTimeout(function() {
            $('#feedback').slideUp();
            updatePlayed();
            updateScore();
            runGame();
        }, 4000);
    } else {
        $('#feedback').slideDown().delay(1500).slideUp();
    }
}

/**
 * Get a random integer between
 * min (inclusive) and max (inclusive)
 */
function getRandomNum(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * Show new min and max values to user
 */
function showRanges(min, max) {
    $('#from').html(min);
    $('#to').html(max);
}

/**
 * Update number of tries variable
 * based on passed options
 */
function updateTries(options) {
    if (options.add) {
        tries++;
        $('#tries').html(tries);
    } else if (options.reset) {
        tries = 0;
        $('#tries').html(tries);
    }
}

/**
 * Update total games played
 */
function updatePlayed() {
    played++;
    $('#played').html(played);
}

/**
 * Calculate and update score
 */
function updateScore() {
    if (tries >= 18) {
        score += 10;
    } else {
        score += 100 - ((tries - 1) * 5);
    }
    $('#score').html(score);
}

/**
 * Get the highest number in
 * an array of numbers
 */
function getHighestNumInArray(array) {
    var highest = array[0];
    for (var i = 0; i < array.length; i++) {
        if (array[i] > highest) {
            highest = array[i];
        }
    }
    return highest;
}

/**
 * Check if a number is between
 * given min and max (inclusive)
 */
function isInRange(number, min, max) {
    if (number >= min && number <= max) {
        return true;
    }
    return false;
}

/**
 * Little cheat function
 * (used for testing)
 */
function gimme() {
    $('#display').val(secret);
    return secret;
}
